﻿using Castle.Core.Internal;
using EPiServer.Core;
using EPiServer.DataAbstraction;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Reflection;
using System.Web;

namespace EpiserverStandard.Web.Models.Blocks
{
    public abstract class BaseBlockData : BlockData
    {
        public override void SetDefaultValues(ContentType contentType)
        {
            PropertyInfo[] properties = GetType().BaseType.GetProperties();
            foreach(var property in properties)
            {
                var defaultValueAttribute = property.GetAttribute<DefaultValueAttribute>();
                if(defaultValueAttribute != null)
                {
                    this[property.Name] = defaultValueAttribute.Value;
                }
            }
            base.SetDefaultValues(contentType);
        }
    }
}